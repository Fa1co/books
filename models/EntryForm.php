<?php
/**
 * Created by PhpStorm.
 * User: php1-4
 * Date: 06.05.2019
 * Time: 17:45
 */
namespace app\models;

use yii\base\Model;

class EntryForm extends Model

{

    public $name;

    public $email;

    public function rules()

    {

        return [

            [['name', 'email'], 'required'],

            ['email', 'email'],

        ];

    }

}